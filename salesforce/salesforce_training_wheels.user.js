// ==UserScript==
// @name         SFDC Training Wheels
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  try to take over the world!
// @author       You
// @match        https://gitlab.my.salesforce.com/*
// @icon         https://www.google.com/s2/favicons?sz=64&domain=salesforce.com
// @downloadURL   https://gitlab.com/rrelax/userscripts/-/raw/main/salesforce/salesforce_training_wheels.user.js
// @installURL    https://gitlab.com/rrelax/userscripts/-/raw/main/salesforce/salesforce_training_wheels.user.js
// @updateURL     https://gitlab.com/rrelax/userscripts/-/raw/main/salesforce/salesforce_training_wheels.user.js
// @grant GM_xmlhttpRequest
// ==/UserScript==

(function () {
    'use strict';

    function isNullOrUndefined(item) {
        return item == undefined || item == null;
    }

    // Find all <td> elements on the page
    const tdList = document.getElementsByTagName("td");

    // Loop through each <td> element
    for (let i = 0; i < tdList.length; i++) {
        const td = tdList[i];

        // Check if the current <td> element contains the text "Account Owner (Text)"
        if (td.innerText === "Account Owner (Text)") {

            // Get the next sibling <td> element
            const accountOwnerTd = td.nextElementSibling;

            // Get the account owner name from the <td> element
            const accountOwnerName = accountOwnerTd.innerText;

            // Create the link
            const link = document.createElement("a");
            link.href = `https://gitlab.my.salesforce.com/_ui/search/ui/UnifiedSearchResults?searchType=2&sen=001&sen=068&sen=003&sen=500&sen=005&sen=006&sen=a1j&str=Carrie+Hipwell#!/fen=005&initialViewMode=detail&str=${encodeURIComponent(accountOwnerName)}`;
            link.textContent = accountOwnerName;

            // Replace the text in the <td> element with the link
            accountOwnerTd.innerHTML = "";
            accountOwnerTd.appendChild(link);

            // Stop looping through <td> elements since we've found what we're looking for
            break;
        }
    }
    setTimeout(() => {
        console.log("Checking if Opp page");
        let pageType = document.querySelector("h1.pageType");
        if (isNullOrUndefined(pageType)) {
            console.log(`PageType set to '${pageType} currently, stopping execution early...`);
            return;
        } else {
            console.log(`PageType set to '${pageType}, with innerText: '${pageType.innerText}'.`);
        }
        if (pageType.innerText == "Opportunity") {
            let firstEmptyLabelElement = document.querySelector(".labelCol.empty")
            let firstEmptyDataElement = document.querySelector(".dataCol.empty");
            if (!isNullOrUndefined(firstEmptyLabelElement) && !isNullOrUndefined(firstEmptyDataElement)) {
                firstEmptyLabelElement.innerText = "Zuora subscription name:";
                firstEmptyDataElement.innerText = "Resolving, please wait.";
                if (document.body.innerText.includes("Sent to Z-Billing")) {
                    const searchTerm = "Quotes";
                    let foundQuotesHeader = null;
                    let allElements = document.querySelectorAll("*");
                    for (let i = 0; i < allElements.length; i++) {
                        let current = allElements[i];
                        if (current.innerText === searchTerm) {
                            if (current.tagName == "H3") {
                                foundQuotesHeader = current;
                                break;
                            }
                        }
                    }
                    if (foundQuotesHeader != null) {
                        let closestSection = foundQuotesHeader.closest(".bPageBlock");
                        if (!isNullOrUndefined(closestSection)) {
                            let anchorToQuote = closestSection.querySelector("a:not(.linkCol)")
                            if (!isNullOrUndefined(anchorToQuote)) {
                                GM_xmlhttpRequest({
                                    method: 'GET',
                                    url: anchorToQuote.href,
                                    onload: function (response) {
                                        let parser = new DOMParser();
                                        let responseDOM = parser.parseFromString(response.responseText, "text/html");
                                        let dataCells = responseDOM.querySelectorAll(".labelCol");

                                        for (let rowIndex = 0; rowIndex < dataCells.length; rowIndex++) {
                                            let currentCell = dataCells[rowIndex];
                                            if (currentCell.innerText.includes("Subscription Name")) {
                                                let subscriptionName = currentCell.nextElementSibling.innerText;
                                                console.log(`Found subscription name: ${subscriptionName}`);

                                                firstEmptyLabelElement.innerText = "Zuora subscription name:";
                                                let zuoraSearchAnchor = document.createElement("a");
                                                zuoraSearchAnchor.target = "_blank";
                                                zuoraSearchAnchor.href = `https://www.zuora.com/platform/apps/search?searchTerm=${subscriptionName}&searchObjectType=subscription`;
                                                zuoraSearchAnchor.innerText = subscriptionName;
                                                firstEmptyDataElement.innerText = "";
                                                firstEmptyDataElement.appendChild(zuoraSearchAnchor);

                                                break;
                                            }
                                        }
                                    },
                                    onerror: function (response) {
                                        console.log(`onerror triggered`);
                                        console.log(response);
                                    }
                                });
                            }
                        }
                    }
                }
            } else {
                console.log("Opportunity not sent to Zuora");
            }
        } else {
            console.log(`Not opportunity - Page Type: '${pageType}'`);
        }
    }, 700);

})();
