// ==UserScript==
// @name [cDot] Linked user account resolver
// @match https://customers.gitlab.com/admin/customer/*
// @grant GM_xmlhttpRequest
// ==/UserScript==

(function() {
  // Find the <span> element with class "uid_field"
  const uidField = document.querySelector('.uid_field');

  // If no element was found, exit the function
  if (!uidField) return;

  // Find the <dt> element that contains the <span> element
  const dtElement = uidField.closest('dt');

  // Find the <dd> element that is a sibling of the <dt> element
  const valueCell = dtElement.nextElementSibling;

  // If the innerHTML of the valueCell element contains a "-", exit the function
  if (valueCell.innerHTML.includes('-')) return;

  // Store the innerHTML of the valueCell element in a variable named "userId"
  const userId = valueCell.innerHTML;

  // Perform a HTTP GET request to the specified URL
  GM_xmlhttpRequest({
    method: 'GET',
    url: `https://gitlab.com/api/v4/users/${userId}`,
    onload: function(response) {
      // Parse the response text as JSON
      const json = JSON.parse(response.responseText);

      // If the JSON object contains an "error" key, print the object to the console and exit the function
      if (json.error) {
        console.log(json);
        return;
      }

      // If the JSON object contains a "username" key, create an anchor element
      const anchor = document.createElement('a');

      // Set the innerHTML of the anchor element to the value of the "username" key in the JSON object
      anchor.innerHTML = json.username;

      // Set the href attribute of the anchor element to the specified URL
      anchor.href = `https://gitlab.com/${json.username}`;

      // Append the anchor element to the valueCell element
      valueCell.appendChild(anchor);
    },
  });
})();
