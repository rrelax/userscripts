# Customer Portal Userscripts

The scripts are broken into several sections:

- This main directory containing the `Customer Portal Training Wheels` userscript - this is the main userscript
- The **Deprecated** directory, containing scripts that are no longer functional or useful
- The **Experimental** directory, containing scripts that aren't actively maintained or were intended to prototype functionality

The **training wheels** script should run on every Customer Portal Admin page - older scripts may need a hard refresh in your browser window before they are enabled (another approach is to open pages you'd like to run the userscripts on in new tabs).


## Training Wheels

### Customer Page functionality

- Adds a new section to the top of a customer profile (`/admin/customer/*`) listing:
   - The customers GitLab.com account (if linked)
   - The customer's Zuora acccount (if they are a member of a Billing Account)
   - The customer's SFDC account (if they are a member of a Billing Account)
   - (Triggered by clicking the `Parse Customer` button that the userscript adds to the navbar)



## Installation

Requirements:
- A userscript manager (I use [TamperMonkey](https://www.tampermonkey.net/), however there are other options!)
- Customer Portal admin-role access

Simply [visit this link](https://gitlab.com/rrelax/userscripts/-/raw/main/customer-portal/cdot-training-wheels.user.js) and your userscript should offer to install the Customer Portal Training Wheels userscript - to update it in future, just return to this link!

## Experiments

### Customer Editing Flags

**Please note:**

- This is a prototype for testing purposes; while it appears to work there may be side effects inside the Portal that i'm not aware of
- The userscript will not run automatically when you navigate to the **Edit** page - either open the Edit page in a new tab, or refresh while viewing the edit page and the flags will be revealed.

