// ==UserScript==
// @name         Customer Portal Training Wheels
// @namespace    https://gitlab.com/rrelax
// @version      0.1
// @description  Adds some useful functionality to the Customer Portal interface
// @match        https://customers.gitlab.com/admin/*
// @run-at       document-end
// @grant GM_xmlhttpRequest
// ==/UserScript==

(function() {
    // Function to add the alert button to the navbar

    function addServiceShortcuts() {
        const billingAccountField = document.querySelector('.billing_accounts_field');
        if(!billingAccountField) return;
        const anchor = billingAccountField.querySelector("a");
        if(!anchor) return;

        const requestURL = anchor.href;
         GM_xmlhttpRequest({
            method: 'GET',
            url: requestURL,
            onload: function(response) {
                let responseJSON = JSON.parse(response.response);
                console.log(responseJSON);
                if(!responseJSON) return;

                const zuoraAccountID = responseJSON.zuora_account_id;
                const zuoraAccountName = responseJSON.zuora_account_name;
                const sfdcAccountID = responseJSON.salesforce_account_id;

                let zuoraElement = document.createElement("a");
                zuoraElement.href = `https://www.zuora.com/apps/CustomerAccount.do?method=view&id=${zuoraAccountID}`;
                zuoraElement.innerHTML = `Zuora account: '${zuoraAccountName}'`;
                zuoraElement.target = "_blank";

                let sfdcElement = document.createElement("a");
                sfdcElement.href = `https://gitlab.my.salesforce.com/${sfdcAccountID}`;
                sfdcElement.innerHTML = `SFDC account`;
                sfdcElement.target = "_blank";


                let accountInformationContainer = document.querySelector(".list-group");
                if(!accountInformationContainer) {
                    console.log("Could not find .list-group element, exiting early...");
                    return;
                }

                let target = document.getElementById("generatedItemsMarker");
                target.appendChild(zuoraElement);
                target.appendChild(sfdcElement);
            }
         });
    }

    function resolveUID(){
          const uidField = document.querySelector('.uid_field');

          // If no element was found, exit the function
          if (!uidField) return;

            const cardBody = uidField.querySelector('.card-body');
            if (cardBody) {
                // If the innerHTML of the cardBody element contains a "-", exit the function
                  if (cardBody.innerHTML.includes('-')) return;
                  const userId = cardBody.innerHTML.trim();
                const requestURL = `https://gitlab.com/api/v4/users/${userId}`;
                console.log(`Requesting: ${requestURL}`);
                GM_xmlhttpRequest({
                    method: 'GET',
                    url: requestURL,
                    onload: function(response) {
                      // Parse the response text as JSON
                      const json = JSON.parse(response.responseText);

                      // If the JSON object contains an "error" key, print the object to the console and exit the function
                      if (json.error) {
                        console.log(json);
                        return;
                      }

                      // If the JSON object contains a "username" key, create an anchor element
                      const anchor = document.createElement('a');

                      // Set the innerHTML of the anchor element to the value of the "username" key in the JSON object
                      anchor.innerHTML = `GitLab Account: ${json.username}`;

                      // Set the href attribute of the anchor element to the specified URL
                      anchor.href = `https://gitlab.com/${json.username}`;

                      // Append the anchor element to the valueCell element
                      cardBody.appendChild(anchor);

                        let target = document.getElementById("generatedItemsMarker");
                        target.appendChild(anchor);
                    },
                  });
            }

    }

    function getCardBody(element) {
        return element.querySelector(".card-body");
    }

    function createCustomerCard(headingText, bodyId){
        let summaryContainer = document.createElement("div");
        summaryContainer.className = "list-group-item border-0 string_type summary_field";
        let summaryContainer_card = document.createElement("div");
        summaryContainer_card.className = "card";
        let summaryContainer_heading = document.createElement("h5");
        summaryContainer_heading.className = "card-header bg-light";
        summaryContainer_heading.innerText = headingText;

        let summaryContainer_body = document.createElement("div");
        summaryContainer_body.className = "card-body";
        summaryContainer_body.id = bodyId;
        summaryContainer_body.style.display = "flex";
        summaryContainer_body.style.flexDirection = "column";

        summaryContainer_card.appendChild(summaryContainer_heading);
        summaryContainer_card.appendChild(summaryContainer_body);

        summaryContainer.appendChild(summaryContainer_card);
        return summaryContainer
    }

    function parseCustomerPage() {
        const currentUrl = window.location.href;
        console.log(currentUrl);
        if(currentUrl.indexOf("admin/customer/") == -1 ){ //TODO: This is too open and will run on the Customers index, needs a regex to match actual customer URL path vars
            alert("This button will only work on Customer pages!");
            return;
        }

        let summaryContainer = createCustomerCard("Generated Summary", "generatedItemsMarker");

        let listGroup = document.querySelector(".list-group");
        listGroup.insertBefore(summaryContainer, listGroup.firstChild);

        resolveUID();
        addServiceShortcuts();
    }

    function addAlertButton() {
        const navbar = document.querySelector('.navbar-nav.ms-auto.root_links');
        if (!navbar) {
            console.log('Navbar not found');
            return;
        }

        const alertButton = document.createElement('button');
        alertButton.textContent = 'Parse Customer';
        alertButton.style.marginLeft = '10px';
        alertButton.id="alertButton";
        alertButton.addEventListener('click', parseCustomerPage);

        if (navbar.hasChildNodes()) {
            let navbarChildren = navbar.childNodes;
            for (const node of navbarChildren) {
                if(node.id == "alertButton"){
                    return;
                }
            }
        }

        navbar.insertBefore(alertButton, navbar.firstChild);
    }

    // Add the alert button after a 3-second delay on the initial page load
    setTimeout(addAlertButton, 3000);

    // Use MutationObserver to detect changes to the page and re-apply the script as necessary
    const observer = new MutationObserver(function(mutations) {
        for (let i = 0; i < mutations.length; i++) {
            const mutation = mutations[i];

            if (mutation.type === 'childList' && mutation.addedNodes.length > 0) {
                // If the added nodes include the navbar, re-apply the script
                const navbar = document.querySelector('.navbar-nav.ms-auto.root_links');
                if (navbar) {
                    observer.disconnect();
                    addAlertButton();
                    observer.observe(document.documentElement, {childList: true, subtree: true});
                    break;
                }
            }
        }
    });
    observer.observe(document.documentElement, {childList: true, subtree: true});
})();
