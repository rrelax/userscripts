// ==UserScript==
// @name Summarise Tickets
// @match https://gitlab.zendesk.com/agent/filters/*
// @match https://gitlab.zendesk.com/agent/filters/
// @grant GM_registerMenuCommand
// @grant GM_setClipboard
// @author       @rrelax
// @version      0.1
// @require https://cdn.jsdelivr.net/combine/npm/@violentmonkey/dom@1,npm/@violentmonkey/ui@0.6
// @namespace     https://gitlab.com/rrelax/userscripts
// @homepageURL   https://gitlab.com/rrelax/userscripts/-/tree/main/zendesk
// @downloadURL   https://gitlab.com/rrelax/userscripts/-/raw/main/zendesk/queue-snapshot.script.js
// @installURL    https://gitlab.com/rrelax/userscripts/-/raw/main/zendesk/queue-snapshot.script.js
// @updateURL     https://gitlab.com/rrelax/userscripts/-/raw/main/zendesk/queue-snapshot.script.js
// ==/UserScript==

GM_registerMenuCommand('Summarise', summarizeTickets);

function toast(message) {
    VM.showToast(message);
  }


function summarizeTickets() {
  var viewsTicketTable = document.getElementById('views_views-ticket-table');
  var timeElements = viewsTicketTable.querySelectorAll('time:not(.react)');
  var rows = [];
  var summaries = [];

  for (var i = 0; i < timeElements.length; i++) {
    var trAncestor = timeElements[i].closest('tr');
    rows.push(trAncestor);
  }

  for (var rowIterator = 0; rowIterator < rows.length; rowIterator++) {
    var summary = {};
    var aTag = rows[rowIterator].querySelector('a');
    summary.path = aTag.getAttribute('href');
    summary.SLAtime = timeElements[rowIterator].innerHTML;
    summary.title = aTag.innerHTML;
    summaries.push(summary);
  }

  var stringSummary = "> Press command + shift + F in Slack to format this in markdown\n\n*This is a snapshot, [consult the queue as the SSOT](https://gitlab.zendesk.com/agent/filters/360075980400)* [made via [userscript](https://gitlab.com/rrelax/userscripts/-/blob/main/zendesk/queue-snapshot.user.js)]\n\n";
  for (var summaryIterator = 0; summaryIterator < summaries.length; summaryIterator++) {
        var emoji = (summaryIterator < 9) ? (summaryIterator + 1).toString() + '️⃣' : '#️⃣';
      stringSummary += `${emoji} [${summaries[summaryIterator].SLAtime} - ${summaries[summaryIterator].title}](https://gitlab.zendesk.com/agent/${summaries[summaryIterator].path})\n`;
  }

  console.log(stringSummary);
    paste(stringSummary);
}

function paste(text) {
    GM_setClipboard(text, 'text/plain');
    if (text.trim().length > 0) {
      toast('Information is copied');
    } else {
      toast('ERROR: nothing copied');
    }
  }
