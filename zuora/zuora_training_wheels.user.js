// ==UserScript==
// @name         Zuora Training Wheels
// @namespace    https://www.gitlab.com/rrelax/userscripts
// @version      0.2
// @description  Modifies the Zuora UI to be more useful
// @author       @rrelax
// @match        https://www.zuora.com/apps/CustomerAccount.do?method=view&id=*
// @match        https://www.zuora.com/platform/subscriptions/*
// @match        https://www.zuora.com/platform/*
// @downloadURL   https://gitlab.com/rrelax/userscripts/-/raw/main/zuora/zuora_training_wheels.user.js
// @installURL    https://gitlab.com/rrelax/userscripts/-/raw/main/zuora/zuora_training_wheels.user.js
// @updateURL     https://gitlab.com/rrelax/userscripts/-/raw/main/zuora/zuora_training_wheels.user.js
// @grant        none
// ==/UserScript==

(function() {
    'use strict';

    const currentURL = window.location.href;
    if(currentURL.indexOf("CustomerAccount.do") != -1) {
        handleCustomerPage();
    } else {
        if(currentURL.indexOf("platform/subscriptions/") != -1) {
            handleSubscriptionPage();
        }
    }

     setTimeout(function() {
        addShortcutsToNavBar();
    }, 1500);

    function generateSubscriptionSearchURL(searchString) {
      // Check if the string parameter matches the format of a hash
      if (/^[0-9a-f]{32}$/i.test(searchString)) {
          console.log("Appears to match a SHA hash");
        return `https://www.zuora.com/platform/subscriptions/${searchString}`;
      } else {
        // Check if the string parameter starts with space characters, "-", "'", or capital letters
        if (/^[\sA-Z'-]/.test(searchString)) {
            console.log("Contains one of: [' ', '\'', /[A-Z]/]")
            return `https://www.zuora.com/platform/apps/search?searchTerm=${searchString}&searchObjectType=subscription`
        } else {
          // Default case
            console.log("Default case");
            return `https://www.zuora.com/platform/apps/search?searchTerm=${searchString}&searchObjectType=subscription`
        }
      }
    }

    function generateCustomerSearchURL(searchString) {
      // Check if the string parameter matches the format of a hash
      if (/^[0-9a-f]{32}$/i.test(searchString)) {
        return `https://www.zuora.com/apps/CustomerAccount.do?method=view&id=${searchString}`;
      } else {
        // Check if the string parameter starts with space characters, "-", "'", or capital letters
        if (/^[\sA-Z'-]/.test(searchString)) {
            return `https://www.zuora.com/platform/apps/search?searchTerm=${searchString}&searchObjectType=account`
        } else {
          // Default case
            return `https://www.zuora.com/platform/apps/search?searchTerm=${searchString}&searchObjectType=account`
        }
      }
    }

    function createSubscriptionSearch() {
        let container = document.createElement("div");
        container.className = "MuiInputBase-root MuiInputBase-colorPrimary MuiInputBase-fullWidth unified-nav-hx3kf";

        let inputElement = document.createElement("input");
        inputElement.placeholder = "Subscription ID";
        inputElement.type = "text";
        inputElement.id = "subscriptionSearchField";
        inputElement.class = "MuiInputBase-input search-focusable unified-nav-1jhxu0";

        let goButton = document.createElement("button");
        goButton.innerHTML = "Go";

        goButton.addEventListener('click', function() {
          const searchTerm = encodeURIComponent(inputElement.value);

          const url = generateSubscriptionSearchURL(searchTerm);
          window.location.href = url;
        });

        inputElement.addEventListener('keypress', function(event) {
          if (event.keyCode === 13) {
            event.preventDefault(); // Prevent the form from submitting
            goButton.click(); // Trigger the click event of the neighboring button
          }
        });

        container.appendChild(inputElement);
        container.appendChild(goButton);
        return container;
    }

    function createAccountSearch() {
    let container = document.createElement("div");
        container.className = "MuiInputBase-root MuiInputBase-colorPrimary MuiInputBase-fullWidth unified-nav-hx3kf";

        let inputElement = document.createElement("input");
        inputElement.placeholder = "Account ID";
        inputElement.type = "text";
        inputElement.id = "customerSearchField";
        inputElement.class = "MuiInputBase-input search-focusable unified-nav-1jhxu0";

        let goButton = document.createElement("button");
        goButton.innerHTML = "Go";

        goButton.addEventListener('click', function() {
          const searchTerm = encodeURIComponent(inputElement.value);

          const url = generateCustomerSearchURL(searchTerm);
          window.location.href = url;
        });

        inputElement.addEventListener('keypress', function(event) {
          if (event.keyCode === 13) {
            event.preventDefault(); // Prevent the form from submitting
            goButton.click(); // Trigger the click event of the neighboring button
          }
        });

        container.appendChild(inputElement);
        container.appendChild(goButton);
        return container;
    }

    function findNavbar() {
        let result = document.querySelector("z-unified-nav-v2");

        if (result != undefined && result != null ) {
            const shadowRoot = result.shadowRoot;
            let navbar = shadowRoot.querySelector(".unified-nav-14b4a8m");

            if(navbar != undefined && navbar != null ) {
                    console.log("Found navbar!");
                return navbar;
            } else {
                    console.log("Found shadowRoot but could not find navbar");
            }
        }
        return null
    }

    function addShortcutsToNavBar() {
        let navbarElement = findNavbar();
        if(navbarElement != null && navbarElement != undefined ) {
            console.log("Check passed - found navbar, adding shortcuts");

            let customersSearch = createAccountSearch();
            let subscriptionSearch = createSubscriptionSearch();
            navbarElement.appendChild(customersSearch);
            navbarElement.appendChild(subscriptionSearch);
        } else {
            console.log("Navbar not found, not adding shortcuts (className may have changed)");
        }

    }

    function handleSubscriptionPage(){
        //alert("Subscription page! TODO")
    }

    function handleCustomerPage() {
         // Find the div with id "account_key_contacts"
        const accountKeyContactsDiv = document.querySelector('#account_key_contacts');

        // Find all span elements within the div
        let spans = accountKeyContactsDiv.querySelectorAll('span');

        // Log the text content of each span to the console

        spans = Array.from(spans).filter(item => item.innerText.indexOf("@")!=-1);

        spans.forEach(span => {
            const emailAddress = span.textContent.trim();
            const domainPart = getEmailDomain(emailAddress);
            console.log(span.textContent);

            // Find the parent of the span and the outer parent of that parent node
            const parent = span.parentNode;
            const outerParent = parent.parentNode;

            // Find the last div with class "z-innerBlock" in the outer parent
            const innerBlock = outerParent.querySelectorAll('.z-innerBlock');
            const lastInnerBlock = innerBlock[innerBlock.length - 1];

            // Create the search shortcuts
            const emailSearchButton = createButton("cDot - email", `https://customers.gitlab.com/admin/customer?model_name=customer&query=${emailAddress}`);
            const domainSearchButton = createButton("cDot - domain", `https://customers.gitlab.com/admin/customer?model_name=customer&query=${domainPart}`);

            lastInnerBlock.appendChild(emailSearchButton);
            lastInnerBlock.appendChild(domainSearchButton);
        });
    }

    function getEmailDomain(email) {
        return email.split('@')[1];
    }

    function createButton(labelText, href) {
        const buttonAnchor = document.createElement('a');
        const spanChild = document.createElement('span');
        buttonAnchor.style.border = "1px solid #f00";
        buttonAnchor.href = href;
        buttonAnchor.target = "_blank";
        buttonAnchor.className = "small-block-button";
        spanChild.innerText = labelText;
        buttonAnchor.appendChild(spanChild);
        return buttonAnchor;
    }

})();
