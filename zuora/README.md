## Zuora userscript

This script acts on several types of Zuora pages:

1. The navbar
1. Customer Account pages
1. Subscription pages

### Effects

#### Navbar

This userscript adds two search fields to the navbar for searching **Accounts** and **Subscriptions**.

Each search field can either take a plaintext parameter or a account/subscription hash.

If a account/subscription hash is provided, the user will be taken to the page for the respective account/subscription.

Otherwise, the user will be taken to a search results page - depending on the field used, the Account or Subscription tabs will be automatically selected.

#### Customer Account pages

##### Sold-To/Bill-To area
This userscript will add two shortcuts to each contact card on a Customer Account page:

1. A button labelled **`cDot - email`** which links to a Customer Portal search page with the contact's email address as the query
2. A button labelled **`cDot - domain`** which links to a Customer Portal search page with the contact's email domain as the query

Both shortcuts will open the target URL in a new tab/window

### Subscription pages

Currently there is no logic implemented for Subscription pages, however I'm planning to:

1. Add a new card to the top of the page, summarising some critical information across the page

### Screenshots

The navbar, showing the account/subscription search fields:

![Screenshot showing the injected Zuora search fields](./screenshots/navbar.png "Screenshot showing the injected Zuora search fields")

The Sold-To card, showing the injected Customer Portal search shortcuts: (The Bill-To card also has these shortcuts)
![Screenshot showing the injected Customer Portal search shortcuts on the Account contact cards](./screenshots/emailSearch.png " showing the injected Customer Portal search shortcuts on the Account contact cards"){height=300px}